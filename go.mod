module gitlab.com/marek2222/api/go-orders-api-gorm

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
)
